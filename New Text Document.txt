git config --global user.name "Mohammad Sajjad Hossain"
git config --global user.email "mohammadsajjad24385@gmail.com"

Create a new repository

git clone https://gitlab.com/sajjad385/calculator.git
cd calculator
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/sajjad385/calculator.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/sajjad385/calculator.git
git push -u origin --all
git push -u origin --tags
